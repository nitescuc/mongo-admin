#!/bin/bash

if [ "$(ls /config/config.yml)" ]; then
  cp /config/config.yml config.yml
elif [ $CONFIG_URL ]; then
  aws s3 cp --region $S3_REGION $CONFIG_URL config.yml
fi

node app.js
