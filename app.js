var backup = require("mongodb-backup");
var fsp = require("path");
var fs = require("fs");
var yaml = require("js-yaml");
var sch = require("node-schedule");
var AWS = require("aws-sdk");
var mailer = require("nodemailer");

// YAML config
var config = {};
try {
  config = yaml.safeLoad(fs.readFileSync(fsp.join(__dirname, "config.yml"), "utf8"));
  console.log("Starting with config", config);
} catch (e) {
  console.log(e);
}

var tracer = config.tracer ? console.log : null;

// AWS config
var awsConfig = config.AWS || {};
awsConfig.region = awsConfig.region || process.env.S3_REGION;
AWS.config.update(awsConfig);

//
var rootDir = (config.destination && config.destination.root && (fsp.isAbsolute(config.destination.root) ? config.destination.root : fsp.join(__dirname, config.destination.root))) || "/backup";

//
function _error(err) {
    console.error((new Date()).toISOString(), err, err.stack);
}

//
function _doIt() {
    var fileName = ((config.destination && config.destination.prefix) || "backup")  + "_" + (new Date()).toISOString().replace(/:|\./g, "-") + ".tar";
    tracer && tracer("Saving", fileName, "to", rootDir);
    //
    function endBackupCb() {
        tracer && tracer("End backup");
        // send to AWS
        if (config.destination.bucket) {
            var body = fs.createReadStream(fsp.join(rootDir, fileName));
            var s3obj = new AWS.S3({
                params: {
                    Bucket: config.destination.bucket, 
                    Key: fileName
                }
            });
            s3obj.upload({Body: body}).send(function(err, data) { 
                if (err) return console.error(err);
            });
        }
        // cleanup
        fs.readdir(rootDir, function(err, files) {
            if (err) return _error(err);
            if (files.length > config.destination.retain) {
                var diff = files.length - config.destination.retain;
                var ff = files.slice(0, diff);
                ff.forEach(function(it) {
                    tracer && tracer("Unlink", fsp.join(rootDir, it));
                    fs.unlink(fsp.join(rootDir, it), function(err) {
                        if (err) return _error(err);       
                    });
                });
            }
            
        });
    }
    //
    console.log("Scheduled execution", (new Date()).toISOString());
    backup({
        uri: config.mongodb.url,
        root: rootDir,
        callback: endBackupCb,
        tar: fileName
    });
}

function _sendMail(message) {
    //
    var transport = mailer.createTransport(config.notify.server);
    var mailOptions = {
        from: config.notify.from,
        to: config.notify.to,
        subject: config.notify.subject || "Backup raport",
        html: message
    }
    transport.sendMail(mailOptions, function(err, info) {
       if (err) return _error(err); 
    });
}

var args = process.argv.slice(2);

if (args.indexOf("--now") >= 0) _doIt();
else sch.scheduleJob(config.schedule, _doIt);

function _notifyIt() {
    console.log("Notify execution", (new Date()).toISOString());
    // read list from fs
    fs.readdir(rootDir, function(err, files) {
        if (err) return _error(err);
        //
        var fsList = "<h1>Locally saved files:</h1>" + files.join("<br/>");
        // read list from S3
        if (config.destination.bucket) {
            var s3obj = new AWS.S3({
                params: {
                    Bucket: config.destination.bucket
                }
            });
            s3obj.listObjects({
                MaxKeys: 100
            }).send(function(err, data) { 
                if (err) return _error(err);
                //
                var awsList = "<h1>AWS saved files:</h1>" + data.Contents.map(function(elt) {
                    return elt.Key;
                }).join("<br/>");
                //
                _sendMail(fsList + awsList);
            });
        } else _sendMail(fsList);
    });
}

if (config.notify) {
    if (args.indexOf("--notifyNow") >= 0) _notifyIt();
    else sch.scheduleJob(config.notify.schedule, _notifyIt);
}